export const state = () => ({
    layout: {
      head: true,
      search: true,
      category: true,
      foot: true,
      right: true,
    }, // 布局显示
  })
  
  export const mutations = {
    UPDATE_LAYOUT(state, hideLayout){
      hideLayout.forEach(i => {
        state.layout[i.type] = i.show
      })
    }
  }