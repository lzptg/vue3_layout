import { get } from 'lodash';

export const state = () => ({
    isLoading: false,
    demoData: {},
    AllRegion: [],
  })
  
  export const mutations = {
    GET_SERVER_DATA(state, data){
        state.demoData = data
    },
    SET_LOADING(state) {
        state.isLoading = state;
    },
    GET_ALL_REGION(state, payload = []) {
        const result = [];
        const map = {};
        const setChildren = list => {
            for (let item of list) {
                if (map[item.region_id]) {
                    item.children = map[item.region_id];
                    setChildren(item.children);
                }
            }
        };
        payload.forEach(item => {
            if (item.parent_id !== '0') {
                if (!map[item.parent_id]) {
                    map[item.parent_id] = [];
                }
                map[item.parent_id].push(item);
            } else {
                item.children = [];
                result.push(item);
            }
        });
        setChildren(result);
        const [ item ] = result
        state.AllRegion = item.children;
      },
  }

  export const actions = {
    /**
    * 获取基础数据
    * @param {string} ids - 需要获取的用户id（多个以|分割）
    */
    getServerData({ commit, state }, { $api, params={} }) {
        commit('SET_LOADING', true);
        return $api.get('/app/data.json', params).then(response => {
            const { data: { data } } = response
            commit('GET_SERVER_DATA', data);
            commit('SET_LOADING', false);
            return response;
        });
    },
    /**
    * 获取城市基础数据
    * @param {string} 
    */
    getAllRegion({ commit, state }, { $api, params={} }) {
        commit('SET_LOADING', true);
        return $api.get('/neigou/regions/all-region', params).then(response => {
            const { data: { payload: { code, list, info } } } = response
            commit('GET_ALL_REGION', list);
            commit('SET_LOADING', false);
            return response;
        });
    },
  }