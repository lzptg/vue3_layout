import { get } from 'lodash';

export const state = () => ({
    isLoading: false,
    homeData: {
        category: [],
    },
    homeIndex: {},
  })
  
  export const mutations = {
    SET_LOADING(state) {
        state.isLoading = state;
    },
    GET_SERVER_DATA(state, data){
        state.homeData = data
    },
    GET_HOME_INDEX_DATA(state, data){
        state.homeIndex = data
    },
  }

  export const actions = {
    /**
    * 获取基础数据
    * @param {string} ids - 需要获取的用户id（多个以|分割）
    */
    getServerData({ commit, state }, { $api, params={} }) {
        commit('SET_LOADING', true);
        return $api.get('/app/data.json', params).then(response => {
            const { data: { data } } = response
            commit('GET_SERVER_DATA', data);
            commit('SET_LOADING', false);
            return response;
        });
    },
    /**
    * 获取城市基础数据
    * @param {string} 
    */
    getHomeData({ commit, state }, { $api, params={} }) {
        commit('SET_LOADING', true);
        return $api.get('/app/data.json', params).then(response => {
            const { data: { data } } = response
            commit('GET_HOME_INDEX_DATA', data);
            commit('SET_LOADING', false);
            return response;
        });
    },
  }