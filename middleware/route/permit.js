import { get } from 'lodash';

/**
 * 页面权限：根据返回用户数据判断用户是否拥有页面权限（全局中间件）
 *
 * @type {Array}
 */

export default function({ store, route, error, redirect, params, query, req, res }) {
    const access = get(store, 'state.auth.user.userInfo.access')
    const toPath = get(route, 'path')
    if(access && toPath && !access.includes(toPath)) {
        error({
            message: '无权限',
            statusCode: 404
        })
    }
}