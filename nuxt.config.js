Object.assign(process.env, {
  BASE_URL: 'http://127.0.0.1:3010',
  BASE_API: 'http://127.0.0.1:3010',
  npm_package_name: 'DEMO测试',
  npm_package_description: 'demo测试',
})

export default {
  ssr: true, // 是否开启服务端渲染
  target: 'static', // 'static'：对于静态网站 | 'server'：用于服务器端渲染
  telemetry: false, // 用户收集反馈
  server: {
    port: 3010,
    host: '0.0.0.0',
  },
  head: {
    title: process.env.npm_package_name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: '//at.alicdn.com/t/font_1744638_3lxthu5sra2.css' }
    ]
  },
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '@/assets/reset.min.css',
    '@/assets/common.css',
  ],
  plugins: [
    { src: '@/plugins/element-ui' },
    { src: '@/plugins/axios/axios' },
    { src: '@/plugins/api' },
    { src: '@/plugins/vue-global' },
    { src: '@/plugins/mock' }, // 模拟数据测试
  ],
  components: true, // 全局组件配置启用
  buildModules: [
  ],
  loading: {
    color: '#1890ff',
  },
  router: {
    middleware: ['auth','layout/index','route/permit'], // 全局路由守卫配置
    mode: 'history', // 'history' 默认方式 | hash
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/style-resources',
    '@nuxtjs/pwa',
    '@nuxt/content',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt',
  ],
  styleResources: {
    // scss: './assets/variables.scss',
    // less: './assets/variables.less',
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/auth/login', method: 'post', propertyName: 'data.token' }, // json数据结构 | propertyName 参数就是返回的 response.data 内我们要获取的内容
          user: { url: '/api/auth/auth', method: 'post', propertyName: 'data' }, // json数据结构
          logout: { url: '/api/auth/logout', method: 'get' },
        }
      }
    },
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/'
    },
    cookie: {
      options: {
        maxAge: 60 * 60 * 24 * 7 // 一周
      }
    },
    localStorage: false
  },
  axios: {
    proxy: true,
    credentials: true,
  },
  proxy: [
    [
      '/api', {
        target: process.env.BASE_API, // 代理地址
        changeOrigin: true,
      },
    ],
    [
      '/app', {
        target: 'https://www.ucharts.cn', // 代理地址
        changeOrigin: true,
        pathRewrite: {
            '^/app': ''
        }
      },
    ],
    [
      '/neigou', {
        target: 'https://api.xinminghui.com', // 代理地址
        changeOrigin: true,
      },
    ]
  ],
  content: {},
  build: {
    vendor: ['axios'],
    extractCSS: { allChunks: true },
    router: {
      extendRoutes (routes, resolve) {
        // 扩展路由，让 nuxt 支持静态页面请求，如 https://www.xxx.cn/index.html
        // routes.push({
        //   name: 'index.html',
        //   path: '/index.html',
        //   component: resolve(__dirname, 'pages/index.vue'),
        // });
      },
    },
  }
}
