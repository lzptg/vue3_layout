/**
 * 拦截器：统一处理 response status !== 200 的情况
 *
 * @type {Array}
 */
 import { MessageBox, Message } from 'element-ui'

const errorHandlerInterceptor = [
    function(responseData) {
        const { status, data } = responseData;
        switch (status) {
            case 200:
                return responseData;
            case 403:
                Message({
                    message: '资源无响应！',
                    type: 'error',
                    duration: 5 * 1000
                  })
                return responseData;
            case 803:
                return responseData;
            default:
                return responseData;
        }
    },
    function(error) {
        return error;
    }
];

export default errorHandlerInterceptor;
