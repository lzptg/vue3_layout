import errorHandlerInterceptor from './error-handler.interceptor';
import fileHandlerInterceptor from './file-handler.interceptor';

export default [
    fileHandlerInterceptor,
    errorHandlerInterceptor,
];
