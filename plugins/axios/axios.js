import { get } from 'lodash';
import { requestInterceptors, responseInterceptors } from './interceptors';

export default function ({ $axios, store, $cookies, redirect }) {
    $axios.defaults.timeout = 30000;
    // 装载所有 request interceptor
    requestInterceptors.forEach(item => {
        $axios.interceptors.request.use(...item);
    });
    // 加载所有 response interceptor
    responseInterceptors.forEach(item => {
        $axios.interceptors.response.use(...item);
    });
}