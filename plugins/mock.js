const Mock = require('mockjs')

// node_modules/mockjs/dist/mock.js
// 8305行后面添加 ↓↓↓↓↓↓↓↓↓
// request.upload.addEventListener is not a function
// MockXMLHttpRequest.prototype.upload = createNativeXMLHttpRequest().upload


Mock.mock(/\/user/, 'get', () => {
  return {
    'list|1-10': [{
      'id|+1': 1
    }]
  }
}
)

Mock.mock(/\/info/, 'post', data => {
  console.log(data)
  return {
    'list|1-10': [{
      'id|+1': 1,
      name: '@cname'
    }]
  }
}
)
// 登陆接口，返回一个 token
Mock.mock(RegExp('/api/auth/login'), 'post', (data) => {
    // console.log('data-login=>>>', data)
    const params = JSON.parse(data.body)
    if(!Object.keys(params).length) {
      return {
        code: 0,
        message: 'error',
        data: {}
      } 
    }
    return {
      code: 200,
      message: 'success',
      data: {
        token: 'awesomeToken-12345678987654321'
      }
    } 
  })
  
  // 获取用户信息接口，返回用户的信息
  Mock.mock(RegExp('/api/auth/auth'), 'post', options => {
    // console.log('options=>>>', options)
    return {
      code: 200,
      message: 'success',
      data: {
        userInfo: {
          name: '王小虎',
          tel: '15484545454654',
          address: '山东省临沂市河东区钩子镇大洼村',
        },
        meanList: [
          { 
            label: '一级类目', path: '', id: 1,
            children: [
              {label: '一级类目-1', path: '/admin/member', id: 1},
              {label: '一级类目-2', path: '', id: 2},
              {label: '一级类目-3', path: '', id: 3},
            ]
          },
          { 
            label: '二级类目', path: '', id: 2,
            children: [
              {label: '二级类目-1', path: '', id: 1},
              {label: '二级类目-2', path: '', id: 2},
              {label: '二级类目-3', path: '', id: 3},
            ]
          },
          { 
            label: '三级类目', path: '', id: 3,
            children: [
              {label: '三级类目-1', path: '', id: 1},
              {label: '三级类目-2', path: '', id: 2},
              {label: '三级类目-3', path: '', id: 3},
            ]
          },
        ],
      }
    }
  })
  
  // 退出接口，一请求即代表退出（后端在 redis 清除 token ，便于克服 JWT 的缺点）
  Mock.mock(RegExp('/api/auth/logout'), 'get', () => {
    return {
      code: 200,
      message: 'success'
    }
  })