/**
 * 手写 promises 实例
 * @param 
 * @description
 */
/**
 * 错误类型实例错误
 * 1）、ReferenceError 引用类型错误
 * 2）、TypeError      数据类型错误
 * 3）、RangeError     数据值不在所允许范围内（递归死循环）
 * 4）、SyntaxError    语法错误
 * promise 定义介绍
 * Promise 是js中进行异步编程的新的解决方案（旧方式为纯回调使用）
 * 语法上是一个构造函数
 * 功能上是一个用对象封装的异步操作并返回其结果
 * 简单介绍promise链式调用使用方式
 */

// js文件 外部引用使用规则
(function(window) {
    const PENDING = 'pending'
    const RESOLVED = 'resolved'
    const REGECTED = 'rejected'

    function Promise(excutor) {
        // 将当前promise对象保存起来
        const self = this
        self.status = 'pending' // 给promise对象指定status属性，初始值为penging
        self.data = undefined  // 给promise对象指定一个用与存储结果数据的属性
        self.callbacks = []    // 每个元素结构： { onResolved(){}, onRejected() {} }

        function resolve(value) {
            // 若当前状态不是pending，直接结束
            if(self.status !== PENDING) return
            // 将状态改为resolved
            self.status = RESOLVED
            // 保存value数据
            self.data = value
            // 保存有待执行callback函数，立即执行异步回调onResolved
            if(self.callbacks.length) {
                setTimeout(function() {
                    self.callbacks.forEach(function(callbacksObj) {
                        callbacksObj.onResolve(value)
                    });
                }, 0);                    
            }
        }
        function reject(reason) {
            // 若当前状态不是pending，直接结束
            if(self.status !== PENDING) return
            // 将状态改为rejected
            self.status = REGECTED
            // 保存value数据
            self.data = reason
            // 保存有待执行callback函数，立即执行异步回调onRejected
            if(self.callbacks.length) {
                setTimeout(function() {
                    self.callbacks.forEach(function(callbacksObj) {
                        callbacksObj.onRejected(reason)
                    }); 
                });                    
            }
        }
        //立即同步执行excutor
        try {
            excutor(resolve, reject)
        } catch (error){
            reject(error)
        }
    }

    Promise.prototype.then = function(onResolve, onReject) {
        onResolve = typeof onResolve === 'function' ? onResolve : value => value
        onReject = typeof onReject === 'function' ? onReject : reason => { throw reason }
        const self = this
        return new Promise(function(resolve, reject) {
            // 调用指定的回调函数
            function handle(callback) {
                /**
                 * 1.如果抛出异常，return的promise就会失败，reason就是error
                 * 2.如果回调函数赶回的不也是promise，return的promise就会成功，value就是返回值
                 * 3.如果回调函数返回时promise，return的promise结果就是这个promise的结果
                 */
                try {
                    const result = callback(self.data)
                    if(result instanceof Promise) {
                        result.then(resolve, reject)
                    } else {
                        reject(result)
                    }
                } catch(error) {
                    reject(error)
                }
            }

           if (self.status === RESOLVED) {
                setTimeout(function() {
                    handle(onResolve)
                });
            } else if(self.status === REGECTED) {
                setTimeout(function(){
                    handle(onReject)
                });
            } else {
                 // 当前状态还是pengding状态，将回调函数保存起来
                 self.callbacks.push({
                    onResolve(value){
                        handle(onResolve)
                    },
                    onReject(reason){
                        handle(onReject)
                    },
                })
            }
        })
    }
    Promise.prototype.catch = function(onReject) {
        return this.then(undefined, onReject)
    }
    Promise.resolve = function(value) {
        if (value instanceof Promise) return value;
        return new Promise ((resolve, reject) => resolve(value));
    }
    Promise.reject = function(reason) {
        return new Promise ((resolve, reject) => reject(reason));
    }
    // 无论成功还是失败都会执行，一般都会传递前一个 promise 的状态，只有在 onFinally 抛出错误（显示抛出或 reject）的时候才会返回一个 rejected 的 promise
    Promise.finally = function(onFinally) {
        return this.then(
            val => Promise.resolve(onFinally()).then(() => val),
            rea => Promise.resolve(onFinally()).then(() => { throw rea; })
        );
    }
    // 当所有 promise 都返回 fulfilled 的时候，它才会返回一个 fulfilled 的 promise，里面包含了对应结果的数组，否则只要一个 promise 返回 rejected，它就会返回一个 rejected 的 promise，其中包含第一个 rejected 的 promise 抛出的错误信息
    Promise.all = function(iterable) { 
        return new Promise ((resolve, reject) => {
            let count = 0;
            let arr = [];
            for (let i = 0, l = iterable.length; i < l; i ++) {
                iterable[i].then(val => {
                    count++;
                    arr[i] = val;
                    if (count === l) {
                        reresolve(arr);
                    }
                }, reject);
            }
        });
    }
    // 只要有一个 promise 返回 fulfilled 或 rejected，它就会返回一个 fulfilled 或 rejected 的 promise
    Promise.reace = function(iterable) {
        return new Promise ((resolve, reject) => {
            for (const p of iterable) {
                p.then(resolve, reject);
            }
        });
    }
    // 只要有一个 promise 成功，就会返回一个成功的 promise，否则返回一个 AggregateError 类型实例的失败 promis
    Promise.any = function(iterable) {
        return new Promise ((resolve, reject) => {
            let count = 0;
            let arr = [];
            for (let i = 0, l = iterable.length; i < l; i ++) {
                iterable[i].then(resolve, rea => {
                    count++;
                    arr[i] = rea;
                    if (count === l) {
                        reject(new AggregateError(arr));
                    }
                });
            }
        });
    };
    // 向外暴露
    window.Promise = Promise
})(window)

