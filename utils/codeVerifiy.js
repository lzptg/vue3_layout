export const chinaReg = /^[\u4e00-\u9fa5]+$/gi
export const numberReg = /^\d{1,}$/
export const phoneReg = /^1[3|4|5|6|7|8|9][0-9]{9}$/
export const emailReg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
export const pwdReg = /^.*(?=.{6,})(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*? ]).*$/
export const VerifyPwd = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/
export const httpReg = /^((https?|ftp|rtsp|mms):\/\/)/
export const idCard = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/
export const mobileReg = /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i

export const isChina = val => chinaReg.test(val)            // 校验汉字
export const isNumber = val => numberReg.test(val)          // 校验纯数字
export const isPhone = val => phoneReg.test(val)            // 校验手机号
export const isEmail = val => emailReg.test(val)            // 校验邮箱号
export const isPwd = val => pwdReg.test(val)                // 最少6位，包括至少1个大写字母，1个小写字母，1个数字，1个特殊字符
export const isVerifyPwd = val => VerifyPwd.test(val)       // 验证6至16位含有字母或数字
export const isHttp = val => httpReg.test(val)              // 验证URL格式
export const isIdCard = val => idCard.test(val)             // 验证身份证号码
export const isMobile = val => mobileReg.test(val)          // 验证是否移动端
